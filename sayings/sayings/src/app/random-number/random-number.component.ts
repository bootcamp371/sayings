import { Component } from '@angular/core';
import { RandomnumberService } from '../providers/randomnumber.service';

@Component({
  selector: 'app-random-number',
  templateUrl: './random-number.component.html',
  styleUrls: ['./random-number.component.css']
})
export class RandomNumberComponent {
  randomNumber: number = 0;

  constructor(private randomNumberService: RandomnumberService) {

  }

onSubmitClick($event:any): void {

  // const saidBy = event.target.value;
  //  const saidBy = this.whoSaidIt;
  //  console.log(saidBy);
  this.randomNumber = this.randomNumberService.generateRandomNumber();

}
}
