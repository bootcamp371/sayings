import { Component, OnInit } from '@angular/core';
import { Saying } from '../models/saying.model';
import { SayingsService } from '../providers/sayings.service';

@Component({
  selector: 'app-sayings',
  templateUrl: './sayings.component.html',
  styleUrls: ['./sayings.component.css']
})
export class SayingsComponent implements OnInit {
  categories: Array<string> = [];
  sayings: Array<Saying> = [];
  matchingSayings: Array<Saying> = [];
  whoSaidIt: string = "";
  constructor(private sayingsService: SayingsService) {

  }
  ngOnInit(): void {
    this.categories = this.sayingsService.getCategories();

  }
  onCategoryChange(event: any): void {

    const selectedCategory = event.target.value;
    console.log(selectedCategory);
    this.matchingSayings = this.sayingsService.getSayingsThatMatchCategory(selectedCategory);
    
  }
  onSaidBy(event: any): void {

   // const saidBy = event.target.value;
    const saidBy = this.whoSaidIt;
    console.log(saidBy);
    this.matchingSayings = this.sayingsService.getSayingsByPerson(saidBy);
    
  }

}
