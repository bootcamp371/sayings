import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SayingsComponent } from './sayings/sayings.component';
import { SayingsService } from './providers/sayings.service';
import { RandomnumberService } from './providers/randomnumber.service';
import { RandomNumberComponent } from './random-number/random-number.component';

const appRoutes: Routes = [
  { path: "", component: SayingsComponent },
  { path: "sayings", component: SayingsComponent },
  { path: "random-number", component: RandomNumberComponent}
  
];
@NgModule({
  declarations: [
    AppComponent,
    SayingsComponent,
    RandomNumberComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [SayingsService, RandomnumberService],
  bootstrap: [AppComponent]
})
export class AppModule { }
