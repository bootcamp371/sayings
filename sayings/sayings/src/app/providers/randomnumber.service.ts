import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomnumberService {
  randomNumber: number = 4;
  constructor() { }

  public generateRandomNumber(): number {
    this.randomNumber = Math.random();
    return this.randomNumber;
  }
}


