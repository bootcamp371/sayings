import { DATE_PIPE_DEFAULT_OPTIONS } from '@angular/common';
import { Injectable } from '@angular/core';
import { Saying } from '../models/saying.model';

@Injectable({
  providedIn: 'root'
})
export class SayingsService {
  categories: Array<string> = [];
  sayings: Array<Saying> = [];

  constructor() {
    this.categories = ["Inspiration", "Money", "Staying Safe", "Statistics"];
    // load the sayings
    this.sayings = [
      new Saying('Inspiration', 'You\'re braver than you believe, and stronger than you seem, and smarter than you think.', 'me'),
      new Saying('Inspiration', 'Nothing is particularly hard if you break it down into small jobs.', 'your mom'),
      new Saying('Staying Safe', 'An apple a day keeps the doctor away.', 'your dad'),
      new Saying('Staying Safe', 'A ship in a harbor is safe, but that is not what ships are for.', 'god'),
      new Saying('Statistics', 'He uses statistics as a drunkenman uses lamp posts... for support rather than illumination.', 'george')]
  }

  public getCategories(): Array<string> {
    return this.categories;
  }

  public getSayings(): Array<Saying> {
    return this.sayings;
  }
  public getSayingsThatMatchCategory(category: string): Array<Saying> {
    let matching =
      this.sayings.filter(s => s.category == category);
    return matching;
  }
  public getSayingsByPerson(name: string): Array<Saying> {
    let matching =
      this.sayings.filter(s => s.person == name);
    return matching;
  }
}
